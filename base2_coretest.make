;Drupal Core
api = 2
core = 7.x
projects[drupal][type] = core

# ; Administration
# projects[admin_menu][subdir] = contrib
# projects[masquerade][subdir] = contrib
# projects[backup_migrate_files][subdir] = contrib
# projects[module_filter][subdir] = contrib
# projects[coffee][subdir] = contrib


# ; Design / Theming
# projects[omega][version] = "4.2"
# projects[fontyourface][subdir] = "contrib"

# projects[block_class][subdir] = contrib
# projects[block_class][version] = "1.2"

# projects[ds][version] = "2.2"
# projects[ds][subdir] = contrib

# projects[fences][subdir] = contrib
# projects[field_formatter_settings][subdir] = contrib
# projects[menu_attributes][subdir] = contrib
# projects[panels_extra_styles][subdir] = contrib
# projects[styleguide][subdir] = contrib
# projects[field_group][subdir] = "contrib"
# projects[field_group][version] = "1.3"

# ;media & files
# projects[stage_file_proxy][subdir] = contrib

# ;development tools
# projects[devel][subdir] = contrib
# projects[devel_themer][subdir] = contrib
# projects[simplehtmldom][subdir] = contrib
# projects[coder][subdir] = contrib
# projects[diff][subdir] = contrib
# projects[media][subdir] = contrib
# projects[shield][subdir] = contrib






# ; Email
# projects[mailsystem][subdir] = contrib
# projects[mimemail][subdir] = contrib
# projects[phpmailer][subdir] = contrib
# projects[mailchimp][subdir] = contrib




# ;UI/Usability
# projects[inline_entity_form][subdir] = "contrib"
# projects[chosen][subdir] = "contrib"
# projects[chosen][version] = "2.0-beta4"

# projects[plup][version] = "1.0-alpha1"
# projects[plup][subdir] = contrib

# ; Fields
# projects[insert][subdir] = contrib
# projects[entityreference][subdir] = contrib

# projects[addressfield][subdir] = contrib
# projects[countries][subdir] = contrib
# projects[date][subdir] = contrib
# projects[email][subdir] = contrib
# projects[link][subdir] = contrib
# projects[field_collection][subdir] = contrib
# projects[video_embed_field][subdir] = contrib
# projects[logintoboggan][subdir] = "contrib"
# projects[logintoboggan][version] = "1.3"


# ;User/Permissions
# projects[fpa][subdir] = contrib
# projects[login_destination][subdir] = contrib
# projects[email_registration][subdir] = contrib
# projects[realname][subdir] = contrib




# ; Helper/Utility Modules
# projects[entity][subdir] = contrib
# projects[libraries][subdir] = contrib
# projects[jquery_update][subdir] = contrib
# projects[uuid][version] = "1.0-alpha3"
# projects[uuid][subdir] = contrib
# projects[advanced_help][subdir] = contrib
# projects[token][subdir] = contrib
# projects[pathauto][subdir] = contrib
# projects[subpathauto][subdir] = contrib
# projects[ctools][subdir] = contrib

# projects[honeypot][version] = "1.15"
# projects[honeypot][subdir] = contrib


# Libraries
# libraries[jquery.cycle][download][type] = "git"
# libraries[jquery.cycle][download][url] = "https://github.com/malsup/cycle.git"
# libraries[jquery.cycle][directory_name] = "jquery.cycle"
# libraries[jquery.cycle][type] = "library"
# libraries[mailchimp][download][type] = "file"
# libraries[mailchimp][download][url] = "http://apidocs.mailchimp.com/api/downloads/mailchimp-api-class.zip"
# libraries[mailchimp][directory_name] = "mailchimp"
# libraries[mailchimp][type] = "library"
# libraries[ckeditor][download][type] = get
# libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.0.1/ckeditor_4.0.1_standard.tar.gz
# libraries[ckeditor][destination] = libraries
# libraries[phpmailer][download][type] = get
# libraries[phpmailer][download][url] = http://phpmailer.apache-extras.org.codespot.com/files/PHPMailer_5.2.2.tgz
# libraries[phpmailer][destination] = libraries
# libraries[plupload][download][type] = "get"
# libraries[plupload][download][url] = "https://github.com/moxiecode/plupload/archive/v2.1.1.zip"
# libraries[plupload][directory_name] = "plupload"
# libraries[plupload][type] = "library"



# ; SEO
# projects[seo_checklist][subdir] = contrib
# projects[checklistapi][subdir] = contrib
# projects[site_map][subdir] = contrib
# projects[site_verify][subdir] = contrib
# projects[page_title][subdir] = contrib
# projects[xmlsitemap][subdir] = contrib
# projects[google_analytics][subdir] = contrib
# projects[globalredirect][subdir] = "contrib"
# projects[globalredirect][version] = "1.5"
# projects[metatag][subdir] = contrib


# projects[redirect][subdir] = "contrib"

# ; Site Building
# projects[nodequeue][subdir] = contrib
# projects[image_focus][subdir] = contrib
# projects[ckeditor][subdir] = contrib
# projects[context][subdir] = "contrib"
# projects[context_omega][subdir] = "contrib"

# projects[print][subdir] = "contrib"



# ; Views
# projects[views][subdir] = contrib
# projects[better_exposed_filters][subdir] = contrib
# projects[views_bulk_operations][subdir] = "contrib"
# projects[views_data_export][subdir] = "contrib"


# project[rules][subdir] = "rules"


# ; add custom profile
# projects[base1][type] = "profile"
# projects[base1][download][type] = "git"
# projects[base1][download][url] = "git@bitbucket.org:rmoger/base1.git"




